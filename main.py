import asyncio
import textwrap
from aiogram import Bot, Dispatcher, types, executor
from aiogram.types import inline_keyboard
from aiogram.utils import markdown as t
import json
from textwrap import dedent
import datetime
import random
import sys

import git
from tribunal import Tribunal
import bcfg
from git import Repo

git_repo = Repo('.')

bot = Bot(bcfg.BOT_TOKEN)
d = Dispatcher(bot)

active_tribunals = []


def is_whitelist(id):
    if bcfg.WL_ENABLED:
        if id in bcfg.WL_CHATS:
            return True
        else: 
            return False
    else:
        return True

async def check_tribunals():
    global active_tribunals
    for tribunal in active_tribunals:
        try:
            await tribunal.update_message()
        except:
            print("Ну и похуй")
        if (tribunal.start_time + tribunal.tribunal_duration.seconds) - datetime.datetime.now().timestamp() <= 0:
            await tribunal.end()
            active_tribunals.remove(tribunal)

async def check_tribunals_every_5_second():
    while True:
        await check_tribunals()
        await asyncio.sleep(5)

@d.message_handler()
async def message_handler(message: types.Message):
    if is_whitelist(message.chat.id) or message.chat.type == "private":
        try:
            cmd = message.text.lower().split(' ')[0]
            if cmd == ".acdbg":
                if message.chat.type != "private":
                    if (await bot.get_chat_member(message.chat.id, message.from_user.id)).is_chat_admin():
                        await message.reply("О, админ, здарова")
                    else:
                        await message.reply("Ты не админ. Пошел нахуй!")
                else:
                    await message.reply("Это не чат!")
            if cmd == ".about":
                await message.reply(dedent(f"""
                <b>Минако Айно | Re-Writed by @fuccsoc on python using aiogram</b>
                <b>Член семьи <a href="https://t.me/leetheartz">1337heartz</a></b>
                <b>Время на сервере:</b> <i>{datetime.datetime.now().isoformat()}</i>
                <b>Коммит:</b> <code>{git_repo.commit().binsha.hex()[:8]}</code>, ALPHA_TEST
                <b>Автор коммита:</b> {git_repo.commit().author}
                <b>Дата и время коммита:</b> {datetime.datetime.fromtimestamp(git_repo.commit().committed_date).isoformat()}
                """), "HTML")
            if cmd == ".утречко":
                await message.reply("Доброе утро! 😄")
            if cmd == ".suicide":
                if message.chat.type != "private":
                    try:
                        suicide_array = ["совершил исекай", "совершил роскомнадзор", "летит в Подольск", "купил хими", "умир", "рипнулся", "суициднулся", "летит из форточки"]
                        muteTime = datetime.datetime.now().timestamp() + datetime.timedelta(minutes=5).seconds
                        await message.reply(f'<a href="tg://user?id={message.from_user.id}">{t.quote_html(message.from_user.full_name)}</a> {random.choice(suicide_array)}', "HTML")
                        await bot.restrict_chat_member(message.chat.id, message.from_user.id, types.ChatPermissions(False), muteTime)
                    except:
                        pass
                else:
                    await message.reply("Это не чат!")
            if cmd == ".changelog":
                await message.reply(textwrap.dedent("""
                    https://teletype.in/@leetheartz/mrbc
                """), "HTML")
            if cmd == ".инлайн":
                ikm = types.InlineKeyboardMarkup(inline_keyboard=[[
                    types.InlineKeyboardButton(text="Inline button", callback_data="testButton"),
                    types.InlineKeyboardButton(text="Inline alert", callback_data="showAlert")
                ]]) 
                await message.reply(text="Inline buttons shown", reply_markup=ikm)
            if cmd == ".delmsg":
                if message.chat.type != "private":
                    if (await bot.get_chat_member(message.chat.id, message.from_user.id)).is_chat_admin():
                        if message.reply_to_message:
                            await bot.delete_message(message.chat.id, message.reply_to_message.message_id)
                            await message.delete()
                        else:
                            await message.reply("Какое сообщение ебнуть?")
                    else:
                        await message.reply("Ты не админ. Пошел нахуй!")
                else:
                    await message.reply("Это не чат!")
            if cmd == ".музыка":
                if len(message.text.split()) > 1:
                    count = message.text.split()[1]
                    try:
                        count = int(count)
                        if count > 3:
                            await message.reply("Слишком дохуя. Будет 3")
                            count = 3
                    except:
                        await message.reply(f"Argument <code>{t.quote_html(count)}</code> is not integer.", "HTML")
                        return
                    for i in range(0, count):
                        auid = random.choice(range(2,596))
                        await message.reply_audio(f"https://t.me/smmusix/{auid}")
                else:
                    auid = random.choice(range(2,596))
                    await message.reply_audio(f"https://t.me/smmusix/{auid}")
            if cmd == ".ids":
                await message.reply(textwrap.dedent(f"""
                <b><i>Имя:</i></b> <code>{t.quote_html(message.from_user.first_name)}</code>
                <b><i>Фамилия:</i></b> <code>{t.quote_html(message.from_user.last_name)}</code>
                <b><i>Юзерка:</i></b> @{message.from_user.username}
                <b><i>ID чата:</i></b> <code>{message.chat.id}</code>
                <b><i>ID юзверя:</i></b> <code>{message.from_user.id}</code>
                <b><i>Тип чата:</i></b> <code>{message.chat.type}</code>
                <b><i><s>с</s>Пермалинк:</i></b> <a href="tg://user?id={message.from_user.id}">{t.quote_html(message.from_user.full_name)}</a>
                """), "HTML")
            if cmd == ".usrid":
                if message.reply_to_message:
                    await message.reply(str(message.reply_to_message.from_user.id))
            if cmd == ".бан" or cmd == ".ban":
                if message.chat.type != "private":
                    if (await bot.get_chat_member(message.chat.id, message.from_user.id)).is_chat_admin():
                        if message.reply_to_message:
                            try:
                                await bot.kick_chat_member(message.chat.id, message.reply_to_message.from_user.id)
                                await message.reply(f'<a href="tg://user?id={message.reply_to_message.from_user.id}">{t.quote_html(message.reply_to_message.from_user.full_name)}</a> идет нахуй с чата!', "HTML")
                            except:
                                await message.reply("У меня нет прав, чтобы отпиздить этого человека")
                        else:
                            await message.reply('Кого баним блять?')
                    else:
                        await message.reply("Ты не админ. Пошел нахуй!")
                else:
                    await message.reply("Это не чат!")
            if cmd == ".разбан" or cmd == ".unban":
                if message.chat.type != "private":
                    if (await bot.get_chat_member(message.chat.id, message.from_user.id)).is_chat_admin():
                        if message.reply_to_message:
                            try:
                                await bot.unban_chat_member(message.chat.id, message.reply_to_message.from_user.id, True)
                                await message.reply(f'Ладно, <a href="tg://user?id={message.reply_to_message.from_user.id}">{t.quote_html(message.reply_to_message.from_user.full_name)}</a> может вернуться в чат.', "HTML")
                            except:
                                await message.reply("У меня нет прав, чтобы отпиздить этого человека")
                        else:
                            await message.reply('А кого разбанить?...')
                            
                    else:
                        await message.reply("Ты не админ. Пошел нахуй!")
                else:
                    await message.reply("Это не чат!")
            if cmd == ".мут" or cmd == ".mute":
                if message.chat.type != "private":
                    if (await bot.get_chat_member(message.chat.id, message.from_user.id)).is_chat_admin():
                        if message.reply_to_message:
                            try:
                                await bot.restrict_chat_member(message.chat.id, message.reply_to_message.from_user.id, types.ChatPermissions(can_send_messages=False))
                                await message.reply(f'<a href="tg://user?id={message.reply_to_message.from_user.id}">{t.quote_html(message.reply_to_message.from_user.full_name)}</a>, заткнись нахуй!', "HTML")
                            except:
                                await message.reply("Админ гони права!")
                        else:
                            await message.reply('Кого мне размутить?')
                    else:
                        await message.reply("Ты не админ. Пошел нахуй!")
                else:
                    await message.reply("Это не чат!")
            if cmd == ".размут" or cmd == ".unmute":
                if message.chat.type != "private":
                    if (await bot.get_chat_member(message.chat.id, message.from_user.id)).is_chat_admin():
                        if message.reply_to_message:
                            try:
                                await bot.restrict_chat_member(message.chat.id, message.reply_to_message.from_user.id, types.ChatPermissions(True, True, True, True, True))
                                await message.reply(f'<a href="tg://user?id={message.reply_to_message.from_user.id}">{t.quote_html(message.reply_to_message.from_user.full_name)}</a> избавился от <s>члена</s> кляпа во рту и может говорить.', "HTML")
                            except:
                                await message.reply("Сука пермишн дай!")
                        else:
                            await message.reply('Кого размутить??...') 
                    else:
                        await message.reply("Ты не админ. Пошел нахуй!")
                else:
                    await message.reply("Это не чат!")
            if cmd == ".бот" or cmd == ".bot" or cmd == ".ping" or cmd == ".пинг":
                await message.reply("Готова!")   
            if cmd == ".tribunal" or cmd == ".трибунал":
                if message.chat.type != "private":
                    if message.reply_to_message:
                        if message.reply_to_message.from_user.id == bot.id:
                            await message.reply("Нахуй сходи!")
                        else:
                            tm = (await message.answer('Да начнется правосудие! <a href="tg://user?id=727314096">Вас к телефону</a>', 'HTML'))
                            tr = Tribunal(
                                message.from_user,
                                message.chat.id,
                                tm.message_id,
                                message.reply_to_message.from_user,
                                bot
                            )
                            await tr.update_message()
                            global active_tribunals
                            active_tribunals.append(
                                tr
                            )
                    else:
                        await message.reply("А кого трибуналим то")
                else:
                    await message.reply("Это не чат!")
            if cmd == ".help" or cmd == ".хелп" or cmd == ".справка" or cmd == ".помощь":
                await message.reply(textwrap.dedent("""
                    .acdbg - check if tou admin
                    .about - bot's about
                    .утречко - доброе утро
                    .suicide - роскомнадзор
                    .changelog - ееее
                    .инлайн - понты с кнопками
                    .delmsg - удалить сообщение
                    .музыка <число> - форвардит музыку с канала @swm1337
                    .ids - получить идентификаторы
                    .usrid - получить ид пользователя (ответ)
                    .бан(.ban)/.разбан(.unban), .мут(.mute)/.размут(.unmute)
                    .трибунал(.tribunal) - правосудие
                """))
            if cmd == "minakostop":
                if message.from_user.id == 1666024811:
                    if len(active_tribunals) == 0:
                        await message.reply("Стопорим бота...")
                        d.stop_polling()
                    else:
                        await message.reply(f"Сейчас идут трибуналы. Список:\n{','.join([f'ChatID: <code>{trib.chat_id}</code>, MessageID: <code>{trib.message_id}</code>' for trib in active_tribunals])}","HTML")
                else:
                    await message.reply("Слышь, нахуй иди, ублюдок. Ты блять охуел? Меня остановить? Пошел нахуй отсюда, выродок ебаный! Мамку свою останови!")
        except:
            print(sys.exc_info())
            await message.reply("Какой-то пиздец произошел")
            await bot.send_message(1666024811, f'пиздец! ошибка!\nЛинк на юзера: <a href="tg://user?id={message.from_user.id}">{t.quote_html(message.from_user.full_name)}</a>\nОбъект сообщения: <code>{t.quote_html(message.as_json())}</code>\nОшибка: <code>{t.quote_html(str(sys.exc_info()))}</code>', "HTML")
    else:
        await bot.send_message(1666024811, f'Новый чат.\nПользователь, который пригласил: хуй знает\nОбъект: <code>{message.chat.as_json()}</code>', "HTML")
        await message.answer(f'Чат не в белом списке. Пишите <a href="tg://user?id=1666024811">ему</a>, но он и так в курсе. ID чата: <code>{message.chat.id}</code>', 'HTML')
        await bot.leave_chat(message.chat.id)

@d.callback_query_handler()
async def cb_handler(cbq: types.CallbackQuery):
    if cbq.data == "testButton":
        await cbq.answer("Ну и нахуя ты ткнул эту кнопку?")
    elif cbq.data == "showAlert":
        await cbq.answer("Хуй с тобой, на алерт!", show_alert=True)
    else:
        trib = None
        for tr in active_tribunals:
            if tr.message_id == cbq.message.message_id:
                trib = tr
        if trib:
            if cbq.data == "vote_for":
                if trib.add_user_for(cbq.from_user):
                    await check_tribunals()
                    await cbq.answer("Вы проголосовали успешно")
                else:
                    await cbq.answer("Иди нахуй!")
            if cbq.data == "vote_against":
                if trib.add_user_against(cbq.from_user):
                    await check_tribunals()
                    await cbq.answer("Вы проголосовали успешно")
                else:
                    await cbq.answer("Иди нахуй!")
            if cbq.data == "cancel_trib":
                if (await bot.get_chat_member(cbq.message.chat.id, cbq.from_user.id)).is_chat_admin() or cbq.from_user.id == trib.initiator.id:
                    await trib.cancel(cbq.from_user)
                    active_tribunals.remove(trib)
                    await cbq.answer("Трибунал отменен!")
                else:
                    await cbq.answer("Иди нахуй!")
        else:
            if cbq.data == "vote_for" or cbq.data == "vote_against" or cbq.data == "cancel_trib":
                await bot.edit_message_text("Трибуналу пиздец по техническим причинам", cbq.message.chat.id, cbq.message.message_id)
                await cbq.answer("Трибуналу пиздец по техническим причинам")
            else:
                await cbq.answer(f'Че ты ткнул? Зарепорчено!')
                await bot.send_message(1666024811, f'Прилетел какой-то левый callback. Чекни.\nЛинк на юзера: <a href="tg://user?id={cbq.from_user.id}">{t.quote_html(cbq.from_user.full_name)}</a>\nОбъект: <code>{cbq.as_json()}</code>', "HTML")

@d.my_chat_member_handler()
async def my_chat_member(member: types.ChatMemberUpdated):
    if not is_whitelist(member.chat.id):
        if member.new_chat_member.status == "member":
            await bot.send_message(1666024811, f'Новый чат.\nПользователь, который пригласил: <a href="tg://user?id={member.from_user.id}">{t.quote_html(member.from_user.full_name)}</a>\nОбъект: <code>{member.chat.as_json()}</code>', "HTML")         
            await bot.send_message(member.chat.id, f'Чат не в белом списке. Пишите <a href="tg://user?id=1666024811">ему</a>, но он и так в курсе. ID чата: <code>{member.chat.id}</code>', 'HTML')
            await bot.leave_chat(member.chat.id)

loop = asyncio.get_event_loop()
loop.create_task(check_tribunals_every_5_second())
executor.start_polling(d, skip_updates=True)
