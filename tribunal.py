from aiogram import types, Bot
import textwrap
import datetime
from aiogram import md as t

class Tribunal:
    inline_markup = types.InlineKeyboardMarkup(inline_keyboard=[
        [
            types.InlineKeyboardButton(text="За", callback_data="vote_for"),
            types.InlineKeyboardButton(text="Против", callback_data="vote_against")
        ],
        [
            types.InlineKeyboardButton(text="Отменить", callback_data="cancel_trib")
        ]
        ])

    def __init__(self, initiator: types.User, chat_id: int, message_id: int, target: types.User, bot: Bot):
        self.initiator = initiator
        self.chat_id = chat_id
        self.message_id = message_id
        self.target = target
        self.bot = bot
        self.users_for = []
        self.users_against = []
        self.start_time = datetime.datetime.now().timestamp()
        self.tribunal_duration = datetime.timedelta(minutes=1)
        self.mute_time = datetime.timedelta(hours=12)

    def add_user_for(self, user: types.User):
        if not (user in self.users_for or user in self.users_against or user == self.target):
            self.users_for.append(user)
            return True
        else:
            return False

    def add_user_against(self, user: types.User):
        if not (user in self.users_for or user in self.users_against or user == self.target):
            self.users_against.append(user)
            return True
        else:
            return False

    async def update_message(self):
        await self.bot.edit_message_text(textwrap.dedent(f"""
            <b><i>Охохо, идет правосудие!!!</i></b>
            <b>Жертва:</b> <b><i><a href="tg://user?id={self.target.id}">{t.quote_html(self.target.full_name)}</a></i></b>
            <b>Начал правосудие:</b> <b><i> <a href="tg://user?id={self.initiator.id}">{t.quote_html(self.initiator.full_name)}</a></i></b>
            <b>Проголосовали за:</b> {', '.join([f'<a href="tg://user?id={user.id}">{user.full_name}</a>' for user in self.users_for])}
            <b>Проголосовали против:</b> {', '.join([f'<a href="tg://user?id={user.id}">{user.full_name}</a>' for user in self.users_against])}
            <b>Секунд до конца:</b> {int((self.start_time + self.tribunal_duration.seconds) - datetime.datetime.now().timestamp())}
            <code>Секунды обновляются при каждом новом голосе или раз в 5 секунд из-за ограничений телеги.</code>
            <a href="tg://user?id=727314096">просила тегать</a>
        """), self.chat_id, self.message_id, parse_mode="HTML", reply_markup=Tribunal.inline_markup)

    async def end(self):
        if len(self.users_for) > len(self.users_against):
            await self.bot.edit_message_text(textwrap.dedent(f"""
                <b><i>Трибунал закончен!</i></b>
                <b><i><a href="tg://user?id={self.target.id}">{t.quote_html(self.target.full_name)}</a> затыкается на {self.mute_time.seconds / 60 / 60 } часов</i></b>
            """), self.chat_id, self.message_id, parse_mode="HTML")
            try:
                await self.bot.restrict_chat_member(self.chat_id, self.target.id, types.ChatPermissions(can_send_messages=False), datetime.datetime.now().timestamp() + self.mute_time.seconds)
            except:
                await self.bot.edit_message_text(textwrap.dedent(f"""
                <b><i>Трибунал закончен!</i></b>
                <b><i><a href="tg://user?id={self.target.id}">{t.quote_html(self.target.full_name)}</a> затыкается на {self.mute_time.seconds / 60 / 60 } часов</i></b>
                
                <u>У меня не получилось лишить участника прав. Возможно, участник администратор или у меня нет прав мутить пользователей</u>
            """), self.chat_id, self.message_id, parse_mode="HTML")
        
        elif len(self.users_for) < len(self.users_against):
            await self.bot.edit_message_text(textwrap.dedent(f"""
                <b><i>Трибунал закончен!</i></b>
                <b><i>Казнь <a href="tg://user?id={self.target.id}">{t.quote_html(self.target.full_name)}</a> отменяется. Расходимся. :(</i></b>
            """), self.chat_id, self.message_id, parse_mode="HTML")
        else:
            await self.bot.edit_message_text(textwrap.dedent(f"""
                <b><i>Трибунал закончен!</i></b>
                <b><i>Шизики не определились... :(</i></b>
            """), self.chat_id, self.message_id, parse_mode="HTML")
        
    async def cancel(self, canceler: types.User):
        await self.bot.edit_message_text(textwrap.dedent(f"""
                <b><i>Трибунал закончен!</i></b>
                <b><i><a href="tg://user?id={canceler.id}">{t.quote_html(canceler.full_name)}</a> отменил трибунал. :(</i></b>
            """), self.chat_id, self.message_id, parse_mode="HTML")
